// Seneca-Webservice.json

let SenecaWeb = require('seneca-web')
let Express = require('express')
let Router = Express.Router
let context = new Router()

let senecaWebConfig = {
    context: context,
    adapter: require('seneca-web-adapter-express'),
    options: {
        parseBody: false // to be able to use bodyParser
    }
}

console.log("Starting webService at port 3000");

let app = Express()
    .use(require('body-parser').json())
    .use(context)
    .listen(3000)

let seneca = require('seneca')()
    .use(SenecaWeb, senecaWebConfig)
    .use(require('./S-api'))
    .client({ type: 'tcp', port: 10201, pin: 'role:math' })

